from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.parsers import JSONParser
#
from .serializers import SneakerSerializer, StockSerializer
from .models import SneakersModel, StockModel


class SneakersAPI(APIView):
    def get(self, request):
        pk = request.GET.get('id')
        sneackers = SneakerSerializer(SneakersModel.objects.all(), many=True)

        if pk:
            sneackers = SneakerSerializer(SneakersModel.objects.get(pk = pk))

        return Response(sneackers.data)

class StockAPI(APIView):
    def get(self, request):
        pk = request.GET.get('id')
        similar_model = StockModel.objects.filter(model = pk, active=True).order_by('size')
        avalible_colors = []
        avalible_size = []

        for obj in similar_model:
            if obj.size not in avalible_size:
                avalible_size.append(obj.size)
            if obj.color.code not in avalible_colors:
                avalible_colors.append(obj.color.code)

        return Response(data={
            u'avalible_colors': avalible_colors,
            u'avalible_size': avalible_size,
        })