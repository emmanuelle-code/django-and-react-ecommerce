from dataclasses import field
from operator import mod
from rest_framework import serializers
from .models import SneakersModel, StockModel

class SneakerSerializer(serializers.ModelSerializer):
    class Meta:
        model = SneakersModel
        fields = '__all__'

class StockSerializer(serializers.ModelSerializer):
    class Meta:
        model = StockModel
        fields = '__all__' 
