from django.contrib import admin
from .models import ColorsModel, SneakersModel, BrandModel, StockModel
# Register your models here.
admin.site.register(ColorsModel)
admin.site.register(SneakersModel)
admin.site.register(BrandModel)
admin.site.register(StockModel)