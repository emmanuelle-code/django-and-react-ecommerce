from django.urls import path
#
from .api import SneakersAPI, StockAPI

urlpatterns = [
    path('sneakers/', SneakersAPI.as_view(), name='sneakers'),
    path('stock/', StockAPI.as_view(), name='stock')
]
