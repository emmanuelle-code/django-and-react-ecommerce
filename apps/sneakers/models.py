import uuid
#
from django.db import models
# Create your models here.

class BrandModel(models.Model):
    brand = models.CharField('Marca', max_length=20)

    def __str__(self):
        return self.brand

class ColorsModel(models.Model):
    name = models.CharField('Name Color', max_length=20)
    code = models.CharField('Code Color', max_length=7)

    def __str__(self):
        return self.code

class SneakersModel(models.Model):
    model = models.CharField('Sneaker Model', max_length=60)
    price = models.IntegerField('Price')
    brand = models.ForeignKey(BrandModel, on_delete=models.CASCADE)
    sold = models.IntegerField('Total vendidos', default=0)

    def __str__(self):
        return self.model

class StockModel(models.Model):
    model = models.ForeignKey(SneakersModel, on_delete=models.CASCADE)
    color = models.ForeignKey(ColorsModel, on_delete=models.CASCADE)
    size = models.CharField('Size', max_length=10)
    n = models.IntegerField('Stock', default=1)
    active =models.BooleanField('Disponible', default=True)