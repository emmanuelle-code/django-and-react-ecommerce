# Generated by Django 3.2.8 on 2022-02-26 02:42

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sneakers', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='stockmodel',
            name='active',
            field=models.BooleanField(default=True, verbose_name='Disponible'),
        ),
    ]
