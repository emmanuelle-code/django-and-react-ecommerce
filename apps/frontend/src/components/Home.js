import React, {useState} from 'react';
import useProduct from '../hooks/useProduct';
import DropdownButton from 'react-bootstrap/DropdownButton'
import Dropdown from 'react-bootstrap/Dropdown';
import useCard from '../hooks/useCard';

export default function Home() {
    const [data, colors, sizes] = useProduct('1');
    const [Modal, showModal, addItemToCard] = useCard();

    const updateData = e => {
        setdata({
            ...data,
            [e.target.name]: e.target.value,
        })
    }
    
    return (
        <div className='container'>
            <Modal />
            <div className='row'>
                <div className='col-12 col-sm-12 col-md-7'>
                    <img className='img-fluid mx-auto' src='https://cdn.shopify.com/s/files/1/0575/1560/6175/products/AirJordan1RetroHighOG-UniversityBlue-WHITE-UNIVERSITYBLUE-BLACK-555088134_600x600.png?v=1626545637'/>
                </div>
                <div className='col-12 col-sm-12 col-md-5'>
                    <h1>{data.model}</h1>
                    <h3>${data.price} MXN</h3>
                    <div className='actions row'>
                        <div className='col-1'>
                            <DropdownButton id="dropdown-basic-button" title=''>
                                {colors.map(color => 
                                    <Dropdown.Item style={{backgroundColor: color, color: '#fff'}} key={color} href="#/action-1" active={ color === '#000' ? true : null}>{color}</Dropdown.Item>
                                )}
                            </DropdownButton>
                        </div>
                        <div className='col-10'>
                            <div className='btn-group me-2'>
                                {
                                    sizes.map(s =>(
                                        <button key={s} onClick={updateData} name='size' value={s} className={s == data.size ? 'btn btn-dark' : 'btn btn-outline-dark'}>{s}</button>
                                    ))
                                }
                            </div>
                        </div>
                        <div className='col-12'>

                        <div className='row mt-3 mb-3'>
                                <div className='col-2'>
                                    <input className='form-control' onChange={updateData} style={{width: '100%'}} name='q' type="number" value={data.q} min={1}/>
                                </div>
                                <div className='d-grid gap-2 col-8 mx-auto'>
                                    <button onClick={() => addItemToCard(data)} className='btn btn-bg btn-dark fluid'><i className="fa-solid fa-cart-circle-plus"></i> Add to card</button>
                                </div>
                                <div className='col-2'><button onClick={showModal}>carrito</button></div>
                            </div>
                        </div>
                    </div>
                    <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>
                </div>
            </div>
        </div>
    )
}
