import React, {  useState, Component } from "react";
import { render } from "react-dom";
import {BrowserRouter, Routes, Route} from 'react-router-dom';
//Components
import Headbar from "./components/Headbar";
import Home from "./components/Home";

export default class App extends Component {
    constructor(props) {
        super(props);
    }
    
    render() {

        return (
            <BrowserRouter>
                <Headbar/>
                <Routes>
                    <Route path="/" element={<Home/>}/>
                </Routes>
            </BrowserRouter>
        );
    }
}

const appDiv = document.getElementById("root");

render(<App />, appDiv);
