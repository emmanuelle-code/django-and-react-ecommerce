import React, {useState, useEffect} from 'react'
import axios from 'axios';

export default function useProduct(id) {
    const [data, setdata] = useState({});
    const [sizes, setsizes] = useState([]);
    const [colors, setcolors] = useState([]);

    useEffect(() => {
        const getData = async () => {
            const res = await axios.get('/api/sneakers/?id=' + id);
            const res2 = await axios.get('/api/stock/?id=' + id);
            setdata(res.data);
            setcolors(res2.data.avalible_colors);
            setsizes(res2.data.avalible_size);
            setdata({
                ...data,
                color: '',
                size: '',
                q: 1,
            })
        }

        getData();

    }, [])
    return [data, colors, sizes]
}
