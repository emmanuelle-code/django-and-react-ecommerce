import React, {useState} from 'react';
import Offcanvas from 'react-bootstrap/Offcanvas';

export default function useCard() {
    const [show, setshow] = useState(false);
    const [cardList, setcardList] = useState([]);

    const showModal = () => {
        setshow(true);
    }

    const addItemToCard = array => {
        setcardList([
            ...cardList,
            array
        ])
    }

    const Modal = () => (
        <Offcanvas show={show} onHide={() => setshow(false)}>
            <Offcanvas.Header closeButton>
                <Offcanvas.Title>Carrito</Offcanvas.Title>
            </Offcanvas.Header>
            <Offcanvas.Body>
                {cardList.map(element => <h1>{ element.model}</h1>)}
            </Offcanvas.Body>
        </Offcanvas>
    )

    return [Modal, showModal, addItemToCard]
}
